/* Commandline program for SYNergy.
 *
 * Call this program with the fds to give the treat.  That is,
 * use their integer values on the cmdline.  This may be useful
 * in a script, as well as in a fork() / exec() combination.
 *
 * This program is run with setuid root.  Not perfect, but to
 * some it might be an alternative to running the daemon or
 * having full root privileges.
 *
 * If you write your own daemons that move rights down from
 * root, you should probably consider passing the fd back up
 * through a UNIX domain socket for added SYNergy.
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>

#include <arpa/inet.h>

#define _SYNERGY_INTERNAL
#include <sys/socketsynergy.h>


int arg2num (char *descr, char *arg, int min, int max) {
	char *endptr = arg;
	long num = strtol (arg, &endptr, 10);
	if (endptr == arg) {
		fprintf (stderr, "%s numeric syntax: %s\n", descr, arg);
		exit (1);
	}
	if ((num < min) || (num > max)) {
		fprintf (stderr, "%s out of range: %d <= %s <= %d\n", descr, min, arg, max);
		exit (1);
	}
	return (int) num;
}


int main (int argc, char *argv []) {
	//
	// Check the commandline
	unsigned long int fd;
	if (argc < 4) {
		fprintf (stderr, "Usage: [SYNERGY_HOPLIMIT=3] %s ipv6addr remote_port <fdnr>...\n", argv [0]);
		exit (1);
	}
	//
	// Parse the ipv6address and remote_port
	struct sockaddr_in6 remote;
	memset (&remote, 0, sizeof (remote));
	remote.sin6_family = AF_INET6;
	remote.sin6_port = htons (arg2num ("remote_port", argv [2], 1, 65535));
	if (inet_pton (AF_INET6, argv [1], &remote.sin6_addr) != 1) {
		perror ("IPv6 address error");
		exit (1);
	}
	//
	// Retrieve the hop limit from the envvar SYNERGY_HOPLIMIT (defaults to 3)
	char *envhop = getenv ("SYNERGY_HOPLIMIT");
	int exitval = 0;
	char *endptr;
	uint8_t hoplimit = (envhop == NULL) ? 3 : arg2num ("$SYNERGY_HOPLIMIT", envhop, 1, 254);
	//
	// Iterate <fdnr>... arguments
	int argi = 2;
	while (++argi < argc) {
		int fdi = arg2num ("File descriptor", argv [argi], 0, INT_MAX);
		if (synergy_privileged (fdi, hoplimit, &remote) != 0) {
			perror ("synergy() failed");
			exitval = 1;
		}
	}
	return exitval;
}
